<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 24.05.2018
 * Time: 18:40
 *
 * @var \app\models\Product $product
 * @var \app\models\Category $category
 */
use yii\widgets\ActiveForm;

?>
<div class="container">
    <?php $form = ActiveForm::begin(); ?>
        <?= $form->field($product, 'name')->textInput([
//            'class' => 'custom-class',
//            'id' => 'custom-id',
//            'something' => 'custom-something',
        ]) ?>
        <?= $form->field($category, 'name')->textInput(['placeholder' => 'some text await']) ?>
        <?= $form->field($product, 'title')->textInput() ?>
        <?= $form->field($product, 'description')->textarea() ?>
        <input type="submit" value="Сабмит">
    <?php ActiveForm::end(); ?>
</div>