<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 02.06.2018
 * Time: 13:30
 */

/* @var $this yii\web\View */
/* @var $cart array */
/* @var $products array */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
    <h1><?= Html::encode($this->title) ?></h1>

    <table class="table">
        <tr>
            <th>#</th>
            <th>Название</th>
            <th>Цена</th>
            <th>Количество</th>
            <th>Сумма</th>
        </tr>
        <?php foreach ($products as $product) { ?>
            <tr>
                <td><?= $product->id ?></td>
                <td><?= $product->name ?></td>
                <td><?= $product->price ?></td>
                <td><?= $cart[$product->id] ?></td>
                <td><?= $cart[$product->id] * $product->price ?></td>
            </tr>
        <?php } ?>
    </table>
    <a href="<?= Url::to(['complete']); ?>" class="btn btn-lg btn-info">Оформить</a>


    <p>
        This is the About page. You may modify the following file to customize its content:
    </p>

    <code><?= __FILE__ ?></code>
</div>
