<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 02.06.2018
 * Time: 13:30
 */

/* @var $this yii\web\View */
/* @var $order \app\models\Order */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
    <h1><?= Html::encode($this->title) ?></h1>

    <?php $form = ActiveForm::begin() ?>
    <?= $form->field($order, 'delivery')->dropDownList($order->getDeliveries(), ['prompt' => 'Выберите способы доставки']) ?>
    <?= $form->field($order, 'payment')->dropDownList($order->getPayments(), ['prompt' => 'Выберите способы оплаты']) ?>
    <?= $form->field($order, 'summ')->textInput() ?>
    <button class="submit">Заказать</button>
    <?php ActiveForm::end() ?>
</div>
