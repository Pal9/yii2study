<?php
use yii\helpers\Url;
?>

<div class="container">
    <h1><?= $this->context->action->uniqueId ?></h1>
    <p>
        This is the view content for action "<?= $this->context->action->id ?>".
        The action belongs to the controller "<?= get_class($this->context) ?>"
        in the "<?= $this->context->module->id ?>" module.
    </p>
    <p>
        You may customize this page by editing the following file:<br>
        <code><?= __FILE__ ?></code>
    </p>
    <table class="table">
        <tr>
            <th>#</th>
            <th>Сумма</th>
            <th></th>
        </tr>
        <?php foreach ($orders as $order) { ?>
            <tr>
                <td><?= $order->id ?></td>
                <td><?= $order->summ ?></td>
                <td><a href="<?= Url::to(['detail', 'order_id' => $order->id ])?>">Информация о заказе</a></td>
            </tr>
        <?php } ?>
    </table>
</div>
