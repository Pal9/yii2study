<?php

namespace app\modules\account\controllers;

use app\models\Order;
use yii\web\Controller;

/**
 * Default controller for the `account` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $orders = Order::find()->where(['user_id' => \Yii::$app->user->id])->all();
        return $this->render('index', [
            'orders' => $orders
        ]);
    }

    public function actionDetail(int $order_id) {
        $order = Order::find()->where(['id' => $order_id, 'user_id' => \Yii::$app->user->id])->all();

        return $this->render('detail', [
            'order' => $order,
        ]);
    }
}
