<?php

use yii\db\Migration;

/**
 * Class m180531_171549_addImages
 */
class m180531_171549_addImages extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('product', 'image', 'string');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180531_171549_addImages cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180531_171549_addImages cannot be reverted.\n";

        return false;
    }
    */
}
