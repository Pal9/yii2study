<?php

use yii\db\Migration;

/**
 * Handles the creation of table `ordrer_product`.
 */
class m180607_160330_create_ordrer_product_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('order_product', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer(),
            'count' => $this->integer(),
            'order_id' => $this->integer()
        ]);

        $this->addForeignKey('product_to_order', 'order_product', 'product_id', 'product', 'id');
        $this->addForeignKey('to_order', 'order_product', 'order_id', 'order', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('ordrer_product');
    }
}
