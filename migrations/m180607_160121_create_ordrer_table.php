<?php

use yii\db\Migration;

/**
 * Handles the creation of table `ordrer`.
 */
class m180607_160121_create_ordrer_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('order', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'delivery' => $this->text(),
            'payment' => $this->integer(),
            'summ' => $this->integer(),
        ]);

        $this->addForeignKey('order_to_user', 'order', 'user_id', 'user', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('ordrer');
    }
}
