<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 05.06.2018
 * Time: 19:26
 */

namespace app\controllers\admin;


use yii\base\Controller;
use yii\filters\AccessControl;

class IndexController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
//                        'controllers' => ['admin/category', 'admin/product', 'index/index'],
                        'roles' => ['admin', 'client']
                    ]
                ]
            ]
        ];
    }

    public function actionIndex() {
        return $this->render('index');
    }

}