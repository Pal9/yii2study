<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 02.06.2018
 * Time: 12:39
 */

namespace app\controllers;

use app\models\Order;
use app\models\OrderProduct;
use app\models\Product;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class CartController extends Controller
{
    public function actionAdd() {
        if (\Yii::$app->request->isAjax) {
            $prodId = \Yii::$app->request->post('prodId');
            $quantity = \Yii::$app->request->post('quantity');

            $cart = \Yii::$app->session->get('cart') ?: [];

            if (in_array($prodId, array_keys($cart))) {
                $cart[$prodId] = $cart[$prodId] + $quantity;
            } else {
                $cart[$prodId] = $quantity;
            }

            \Yii::$app->session->set('cart', $cart);

            \Yii::$app->response->format = Response::FORMAT_JSON;

            return $cart;
        }

        throw new NotFoundHttpException('Доступ запрещён', 404);
    }

    public function actionShow() {
        if (\Yii::$app->request->isGet) {

            $cart = unserialize(\Yii::$app->session->get('cart'));
            var_dump($cart);

        }
        \Yii::$app->response->format = Response::FORMAT_JSON;
    }

    public function actionIndex()
    {
        $cart = \Yii::$app->session->get('cart') ?: [];
        $prodIds = array_keys($cart);
        $products = Product::find()->where(['in', 'id', $prodIds])->all();
        return $this->render('view', [
            'products' => $products,
            'cart' => $cart
        ]);
    }

    public function actionComplete() {
        $cart = \Yii::$app->session->get('cart') ?: [];
        $order = new Order();

        if (Yii::$app->request->isPost && $order->load(Yii::$app->request->post())) {
            $prodIds = array_keys($cart);
            $products = Product::find()->where(['in', 'id', $prodIds])->all();
            $sum = 0;
            foreach ($products as $product) {
                $sum += $product->price * $cart[$product->id];
            }
            $order->summ = $sum;
            $order->user_id = Yii::$app->user->id;
            Yii::$app->session->remove('cart');

            $order->save();

            foreach ($cart as $key => $item) {
                (new OrderProduct([
                    'order_id' => $order->id,
                    'product_id' => $key,
                    'count' => $item,
                ]))->save();
            }
        }

        return $this->render('complete', [
            'order' => $order,
//            'cart' => $cart
        ]);
    }
//
//    public function actionView2()
//    {
//        \Yii::$app->session->addFlash('info', 'ololo');
//        \Yii::$app->session->addFlash('info2', 'ololo');
//        \Yii::$app->session->addFlash('info', 'ololo');
//        \Yii::$app->session->removeFlash('info');
//
//        return $this->render('view2');
//    }

}