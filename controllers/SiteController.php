<?php

namespace app\controllers;

use app\models\Category;
use app\models\Product;
use app\models\User;
use Yii;
use yii\data\Pagination;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{

    /**
     * @var string имя шаблона для страниц данного контроллера
     */
//    public $layout = '';

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * My very first page in Yii
     * @return string
     */
    public function actionPage() {
        $text = 'This is content of page. Let me be in DB.';
        return $this->render('page', [
            'text' => $text,
        ]);
    }

    public function actionProduct(int $id) {
        $product = Product::find()->where(['id' => $id])->one();

        if (!$product) {
            throw new NotFoundHttpException('404');
        }
        return $this->render('product', [
            'product' => $product,
        ]);
    }

    public function actionProductList(?int $id = NULL) : string {
        $products = $id ? Product::find()->where(['category_id' => $id]) : Product::find();
//        $products = Product::find();
        $categories = Category::find()->all();

        $countProducts = clone $products;

        $pages = new Pagination(['totalCount' => $countProducts->count(), 'defaultPageSize' => 1]);
        $products = $products->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        return $this->render('product-list', [
            'products' => $products,
            'categories' => $categories,
            'pages' => $pages
        ]);
    }

    public function actionSingUp() {

        $user = new User();

        if ($user->load(Yii::$app->request->post())) {

            $user->setPassword($user->password);
            $user->save();

            $role = Yii::$app->authManager->getRole('client');
            Yii::$app->authManager->assign($role, $user->id);

            return $this->redirect(['site/login']);
        }

        return $this->render('sign-up', [
            'user' => $user,
        ]);
    }

//    public function actionRoles() {
//        $adminRole = Yii::$app->authManager->createRole('admin');
//        Yii::$app->authManager->add($adminRole);
//        $clientRole = Yii::$app->authManager->createRole('client');
//        Yii::$app->authManager->add($clientRole);
//        $managerRole = Yii::$app->authManager->createRole('manager');
//        Yii::$app->authManager->add($managerRole);

        // привязываем роль админа к пользователю 1:
//        $adminRole = Yii::$app->authManager->getRole('admin');
//
//        $user = User::findOne(1);
//
//        Yii::$app->authManager->assign($adminRole, $user->id);
//    }
}
