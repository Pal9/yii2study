<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 05.06.2018
 * Time: 20:08
 */

namespace app\widgets;


use yii\base\Widget;

class Menu extends Widget
{

    public $message;

    public function init()
    {
    }

    public function run() {
        return $this->render('menu', [
            'message' => $this->message,
        ]);
    }
}